import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Push, PushObject, PushOptions } from '@ionic-native/push';

import { Utils } from '../providers/utils/utils'
import { SessionManagement } from '../providers/sessionManagement/sessionManagement'
import { WebService } from '../providers/webService/webService';

import { Sidemenu } from '../pages/sidemenu/sidemenu';
import { LoginSignupPage } from '../pages/login-signup/login-signup';
// import { MyOrderScreenPage } from '../pages/my-order-screen/my-order-screen';
// import { DashboardPage } from '../pages/dashboard/dashboard';
// import { ServiceSelectPage } from '../pages/service-select/service-select';
// import { OrderDetailPage } from '../pages/order-detail/order-detail';
// import { MyProfilePage } from '../pages/my-profile/my-profile';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public push: Push, public webSvc: WebService, public utils: Utils, private sessionManagement: SessionManagement) {
    platform.ready().then(() => {
      this.pushsetup();
      statusBar.styleDefault();
      splashScreen.hide();
    });
    this.webSvc.getAllActiveService().subscribe(result => {
      if (result) {
        this.utils.activeServices = result.activeServices;
      }
    }, err => {
      // this.utils.showToaster("No Internet");
      console.log(err);
    })
  }

  ngOnInit() {
    let currentRef = this;
    this.sessionManagement.getToken().then(token => {
      if (token) {
        currentRef.rootPage = Sidemenu;
      } else {
        currentRef.rootPage = LoginSignupPage;
      }
    })
  }
  pushsetup() {
    const options: PushOptions = {
      android: {
        sound: true,
        vibrate: true,
        senderID: '934963175357'
      },
      ios: {
        alert: true,
        badge: true,
        sound: true
      },
      windows: {}
    };

    const pushObject: PushObject = this.push.init(options);

    pushObject.on('notification').subscribe((notification: any) => {
      if (notification.additionalData.foreground) {
        // this.playSound();
        this.utils.showToaster("New notification for " + notification.additionalData.serviceType + " in " + notification.additionalData.city);
        // this.utils.showAlert("New notification",false,"You have a new service notification","take me there",function(){
        //   // set page for notification 
        // },function(){
        //   console.log("cancelled notification");
        // })
      }
    });
    pushObject.on('registration').subscribe((registration: any) => {
      //do whatever you want with the registration ID
      this.utils.pushNotification = registration;
      console.log(registration.registrationId);
      //  this.webSvc.storeRegistrationToken(registration.registrationId).subscribe(data=>{
      //    console.log("success");
      //  })
    });

    pushObject.on('error').subscribe(error => alert('Error with Push plugin' + error));
  }
}

