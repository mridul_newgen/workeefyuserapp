import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';
import { Http, XHRBackend, RequestOptions } from '@angular/http';
import { Camera } from '@ionic-native/camera';
import { FileTransfer} from '@ionic-native/file-transfer';
// import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { DatePickerModule, DatePickerDirective } from 'ion-datepicker';

import { MyApp } from './app.component';
import { LoginSignupPage } from '../pages/login-signup/login-signup';
import { MyOrderScreenPage } from '../pages/my-order-screen/my-order-screen';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { ServiceSelectPage } from '../pages/service-select/service-select';
import {ChangePasswordPage} from '../pages/change-password/change-password'
import { Sidemenu } from '../pages/sidemenu/sidemenu';
import { TabsPage } from '../pages/tabs/tabs';
import { OrderDetailPage } from '../pages/order-detail/order-detail';
import { ChangeAddressModalPage } from '../pages/change-address-modal/change-address-modal';
import { AutoCompleteModalPage } from '../pages/auto-complete-modal/auto-complete-modal';
import{AddNewCardPage} from '../pages/add-new-card/add-new-card';
import { Utils } from '../providers/utils/utils';
import { SessionManagement } from '../providers/sessionManagement/sessionManagement';
import { WebService } from '../providers/webService/webService';
import { HttpProvider } from '../providers/http/http';
import { IonicStorageModule } from '@ionic/storage';
import { Push } from '@ionic-native/push';
import {MyProfilePage} from '../pages/my-profile/my-profile';
import { LimitInputDirective } from '../directives/limit-input/limit-input'

@NgModule({
  declarations: [
    MyApp,
    LoginSignupPage,
    MyOrderScreenPage,
    DashboardPage,
    ServiceSelectPage,
    OrderDetailPage,
    ChangeAddressModalPage,
    AutoCompleteModalPage,
    Sidemenu,
    TabsPage,
    MyProfilePage,
    ChangePasswordPage,
    AddNewCardPage,
    LimitInputDirective
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    DatePickerModule,
    IonicStorageModule.forRoot(),
    // HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginSignupPage,
    MyOrderScreenPage,
    DashboardPage,
    ServiceSelectPage,
    OrderDetailPage,
    ChangeAddressModalPage,
    AutoCompleteModalPage,
    Sidemenu,
    TabsPage,
    MyProfilePage,
    ChangePasswordPage,
    AddNewCardPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    FileTransfer,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    Utils,
    SessionManagement,
    WebService,
    DatePickerDirective,
    Push,
    // {
    //   provide: HTTP_INTERCEPTORS,
    //   useFactory: (storageProvider: Storage, utils: Utils) => {
    //     return new HttpProvider(storageProvider, utils);

    //   },
    //   deps: [Storage, Utils],
    //   multi: true
    // }
    {
      provide: Http,
      useFactory: (backend: XHRBackend, defaultOptions: RequestOptions, utils: Utils, sessionManagement: SessionManagement) => {
        return new HttpProvider(backend, defaultOptions, utils, sessionManagement);
      },
      deps: [XHRBackend, RequestOptions, Utils, SessionManagement]
    }
  ]
})
export class AppModule { }
