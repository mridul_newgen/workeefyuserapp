import { Directive, Input } from '@angular/core';

@Directive({
  selector: '[limit-input]', // Attribute selector
  host: {
    '(keypress)': '_onKeypress($event)',
  }

})
export class LimitInputDirective {
  constructor() {
  }
  @Input('limit-input') limitInput;
  _onKeypress(event) {
    let value = event.keyCode || event.charCode || event.which;
    if (this.processKeyCodes(value))
      event.preventDefault();
    else {
      const limit = +this.limitInput;
      if (event.target.value.length === limit)
        event.preventDefault();
    }
  }

  processKeyCodes(value) {
    switch (value) {
      case 101: return true; //e (alphabet)
      case 45: return true; //- (Minus)
      case 43: return true; //+ (Equal)
      case 46: return true; //. (Period)
    }
  }

}