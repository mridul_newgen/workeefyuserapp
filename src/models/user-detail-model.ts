
import { BasicDetail } from "./basic-detail";
import { PaymentDetail } from "./payment-detail";
import { HouseDetail } from "./house-detail";
export class UserDetailModel {
    BasicDetail: BasicDetail;
    PaymentDetail: [PaymentDetail];
    HouseDetail:HouseDetail;
    UserUID: string;
    UserRole: string;
    
    constructor() {
        this.BasicDetail = new BasicDetail();
        this.HouseDetail = new HouseDetail();
        this.PaymentDetail = [new PaymentDetail()];
    }
}