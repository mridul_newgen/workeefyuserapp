export class BasicDetail {
    FirstName: string;
    LastName: string;
    Email: string;
    Password: string;
    Address: string;
    Latitude: number;
    Longitude: number;
    City: string;
    Phone: number;
    PostalCode: number;
    ProfilePic: string;
    ForSocial: boolean;
    Verified?: boolean;

    constructor() {
        //this.Latitude = this.Longitude = "";
    }
}