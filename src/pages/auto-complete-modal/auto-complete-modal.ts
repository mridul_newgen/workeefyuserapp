import { Component, ViewChild, NgZone } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';

declare var google: any;
@Component({
  selector: 'page-auto-complete-modal',
  templateUrl: 'auto-complete-modal.html',
})
export class AutoCompleteModalPage {
  @ViewChild('searchBar') searchBar;
  autocompleteItems;
  autocomplete;

  latitude: number = 0;
  longitude: number = 0;
  geo: any;

  service = new google.maps.places.AutocompleteService();

  constructor(public viewCtrl: ViewController, private zone: NgZone, private params: NavParams) {
    this.autocompleteItems = [];
    this.autocomplete = {
      query: this.params.get('searchedData'),
    };
  }
  ionViewDidLoad() {

  }
  dismiss() {
    this.viewCtrl.dismiss();
  }

  chooseItem(item: any) {
    this.viewCtrl.dismiss(item);
    // this.geo = item;
    // this.geoCode(this.geo);//convert Address to lat and long
  }
  updateSearch() {
    if (this.autocomplete.query == '') {
      this.autocompleteItems = [];
      return;
    }
    let me = this;
    this.service.getPlacePredictions({ input: this.autocomplete.query, componentRestrictions: { country: 'CAN' } }, function (predictions, status) {
      me.autocompleteItems = [];
      me.zone.run(function () {
        if (predictions == undefined) {
          me.autocompleteItems.push("Location not found");
        }
        else {
          predictions.forEach(function (prediction) {
            me.autocompleteItems.push(prediction.description);
          });
        }
      });
    });
  }

  //convert Address string to lat and long
  geoCode(address: any) {
    let geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'address': address }, (results, status) => {
      this.latitude = results[0].geometry.location.lat();
      this.longitude = results[0].geometry.location.lng();
      //alert("lat: " + this.latitude + ", long: " + this.longitude);
    });
  }
}