import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Utils } from './../../providers/utils/utils';
import { WebService } from './../../providers/webService/webService';

@Component({
  selector: 'page-change-password',
  templateUrl: 'change-password.html',
})
export class ChangePasswordPage {
  changePasswordForm: FormGroup;
  constructor(public viewCtrl: ViewController,public navCtrl: NavController, public navParams: NavParams, private utils: Utils, private webSvc: WebService, ) {
  }

  ngOnInit() {
    this.changePasswordForm = new FormGroup({
      newPassword: new FormControl(null, [Validators.required,Validators.minLength(8)])
    });
  }
  closeModal(){
    this.viewCtrl.dismiss();
  }
  updatePassword() {
    this.webSvc.updatePassword(this.utils.encryptPassword(this.changePasswordForm.controls.newPassword.value)).subscribe(result => {
      if (result) {
        if (result && !result.success) {
          this.utils.showToaster("An Error has occurred");
        }
        else {
          this.utils.showToaster("Password Changed Successfully");
          this.viewCtrl.dismiss();
        }
      }
    }, err => {
      this.utils.showToaster("An Error has occurred,Retry after some time Or check your internet connection");
      // this.utils.showToaster(err);
    })

  }

}
