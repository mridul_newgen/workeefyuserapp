import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';
import { ServiceSelectPage } from '../service-select/service-select';
import { MyOrderScreenPage } from '../my-order-screen/my-order-screen';

@Component({
    selector: 'page-tabs',
    templateUrl: 'tabs.html',
})
export class TabsPage {

    tab1Root: any = ServiceSelectPage;
    tab2Root: any = MyOrderScreenPage;
    myIndex: number;

    constructor(navParams: NavParams) {
        // Set the active tab based on the passed index from menu.ts
        this.myIndex = navParams.data.tabIndex || 0;
    }
}