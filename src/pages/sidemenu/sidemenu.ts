import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Nav, Tabs, App } from 'ionic-angular';
import { DashboardPage } from '../dashboard/dashboard';
import { TabsPage } from '../tabs/tabs';
import { MyOrderScreenPage } from '../my-order-screen/my-order-screen';
import { ServiceSelectPage } from '../service-select/service-select';
import { LoginSignupPage } from '../login-signup/login-signup';
import { SessionManagement } from '../../providers/sessionManagement/sessionManagement'
import { MyProfilePage } from '../my-profile/my-profile';

/**
 * Generated class for the Sidemenu page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

export interface PageInterface {
  title: string;
  pageName: any;
  tabComponent?: any;
  index?: number; // -1 for signOut
  icon: string;
}


@Component({
  selector: 'page-sidemenu',
  templateUrl: 'sidemenu.html',
})
export class Sidemenu {
  @ViewChild(Nav) nav: Nav;
  rootPage = DashboardPage;
  pages: PageInterface[] = [
    { title: 'Dashboard', pageName: DashboardPage, icon: 'shuffle' },
    { title: 'Order Service', pageName: TabsPage, tabComponent: ServiceSelectPage, index: 0, icon: 'home' },
    { title: 'My Orders', pageName: TabsPage, tabComponent: MyOrderScreenPage, index: 1, icon: 'create' },
    { title: 'My Profile', pageName: MyProfilePage, icon: 'contact' },
    { title: 'Sign Out', pageName: LoginSignupPage, index: -1, icon: '' },
  ];

  constructor(public navCtrl: NavController, public navParams: NavParams, private app: App, private sessionManagement: SessionManagement) {
  }

  openPage(page: PageInterface) {
    let params = {};
    if (page.index == -1) {
      this.signOut();
      this.navCtrl.setRoot(page.pageName);
    }

    // The index is equal to the order of our tabs inside tabs.ts
    else if (page.index) {
      params = { tabIndex: page.index };
    }
    // The active child nav is our Tabs Navigation
    const tabsNav = this.app.getNavByIdOrName('myTabsNav') as Tabs;
    if (tabsNav && page.index != undefined) {
      // this.nav.getActiveChildNav().select(page.index);
      tabsNav.select(page.index);
      // this.navCtrl.pop();
    } else {
      // Tabs are not active, so reset the root page 
      // In this case: moving to or from SpecialPage
      this.navCtrl.popToRoot();
      this.nav.setRoot(page.pageName);
    }
  }

  signOut() {
    this.sessionManagement.deleteSession();
    //empty all stored values and clear cache
  }

  isActive(page: PageInterface) {
    // Again the Tabs Navigation
    // let childNav = this.nav.getActiveChildNav();
    const childNav = this.app.getNavByIdOrName('myTabsNav') as Tabs;

    if (childNav) {
      if (childNav.getSelected() && childNav.getSelected().root === page.tabComponent) {
        return 'primary';
      }
      return;
    }

    // Fallback needed when there is no active childnav (tabs not active)
    if (this.nav.getActive() && this.nav.getActive().name === page.pageName) {
      return 'primary';
    }
    return;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Sidemenu');
  }

}
