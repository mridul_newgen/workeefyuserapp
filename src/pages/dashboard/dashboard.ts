import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Slides } from 'ionic-angular';


import { WebService } from './../../providers/webService/webService';
import { Utils } from './../../providers/utils/utils';
import{ServiceSelectPage} from './../service-select/service-select'


@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {
  @ViewChild(Slides) slides: Slides;
  lengthOfSlide: number;
  activeServices: any = []
  bestOfferList: any = [
    {
      title1: 'Home Cleaning',
      price1: 20,
      title2: 'Lawn Maintenance',
      price2: 30,
      image1: './assets/images/home-cleaning-test.png',
      image2: './assets/images/lawn-maintenance.jpg',
    },
    {
      title1: 'Snow Removal',
      price1: 30,
      title2: 'Yard Cleaning',
      price2: 30,
      image1: './assets/images/Snow-Removal.jpg',
      image2: './assets/images/yard-cleaning.jpg',
    }
  ]
  workeefyerList: any = [
    {
      name: "sanjay Nagpal",
      profilePic: './assets/icon/dollar.png',
      rating: '5',
    }
  ]
  serviceSelectList: any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, private utils: Utils, private webSvc: WebService) {
  }
  ngOnInit() {
    this.utils.activeServices.length > 0 ? this.activeServices = this.utils.activeServices : this.webSvc.getAllActiveService().subscribe(result => {
      if (result) {
        this.activeServices = result.activeServices;
      }
    }, err => {
      // this.utils.showToaster("No Internet");
      console.log(err);
    })
  }

  nextWorkeefyer() {
    this.slides.slideNext();

  }
  prevWorkeefyer() {
    this.slides.slidePrev();
    // console.log('Current index is', currentIndex);
  }
  openSelectService(serviceName:any){
    this.navCtrl.push(ServiceSelectPage,{serviceName:serviceName})
  }
}
