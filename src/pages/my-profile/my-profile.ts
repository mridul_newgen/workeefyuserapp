import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { NavController, NavParams, ModalController, ActionSheetController, ViewController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';


import { AutoCompleteModalPage } from '../auto-complete-modal/auto-complete-modal';
import { ChangePasswordPage } from '../change-password/change-password';
import { AddNewCardPage } from '../add-new-card/add-new-card'
import { UserDetailModel } from '../../models/user-detail-model';
import { Utils } from './../../providers/utils/utils';
import { WebService } from './../../providers/webService/webService';
import { SessionManagement } from './../../providers/sessionManagement/sessionManagement'


@Component({
  selector: 'page-my-profile',
  templateUrl: 'my-profile.html',
})
export class MyProfilePage {
  imageURI: any;
  previousDefaultCard: any;
  previousDefaultAddress: any;
  updatedItem: string;
  userToken: any;
  profilePic: any;
  profilePicPath: any;
  email: any;
  userData: any = [{
    profilePic: '',
    firstName: '',
    lastName: '',
    phone: '',
    email: '',
    address: [],
    cardDetail: []
  }];
  defaultAddress: any;
  defaultCard: any;
  userCompleteAddress = {
    address: '',
    city: '',
    latitude: 0,
    longitude: 0,
    isDefault: false
  }
  addressObj = {
    city: '',
    latitude: 0,
    longitude: 0,
  }
  public disableName: boolean = true;
  public disablePhone: boolean = true;
  userAddress: any = []
  userCard: any = []
  nameGroup: FormGroup;
  phoneGroup: FormGroup;
  constructor(public navCtrl: NavController, public viewCtrl: ViewController, public navParams: NavParams, private modalCtrl: ModalController, private utils: Utils, private webSvc: WebService, private sessionManagement: SessionManagement, private camera: Camera, public actionSheetCtrl: ActionSheetController, ) {
  }
  ngOnInit() {
    this.nameGroup = new FormGroup({
      firstName: new FormControl(null, [Validators.required, Validators.pattern(this.utils.alphaRegex)]),
      lastName: new FormControl(null, [Validators.required, Validators.pattern(this.utils.alphaRegex)]),
    });
    this.phoneGroup = new FormGroup({
      phone: new FormControl(null, [Validators.required, Validators.pattern(this.utils.phoneRegex)]),
    });
    this.getUserData();
  }

  getUserData() {
    this.sessionManagement.getToken().then(token => {
      if (token) {
        this.userToken = token;
        let uuid = this.utils.decodeToken(token).userId;
        this.webSvc.fetchUserInfo(uuid).subscribe(result => {
          // this.utils._currentUser.userData = result;
          this.userData = result.userData.userData;
          this.processUserData(this.userData);
        }, err => {
          console.log("An error occured")
        });
      } else {
        // currentRef.rootPage = LoginSignupPage;
      }
    });

  }

  processUserData(userData) {
    this.profilePicPath = userData.basicUserInfo ? userData.basicUserInfo.profilePic : '';
    this.profilePic = userData.basicUserInfo ? this.webSvc.API_profilePic_URL + userData.basicUserInfo.profilePic : './assets/images/noProile.png';
    this.nameGroup.setValue({ firstName: userData.basicUserInfo.firstName, lastName: userData.basicUserInfo.lastName });
    this.phoneGroup.setValue({ phone: userData.basicUserInfo.phone })
    this.email = userData.basicUserInfo ? userData.basicUserInfo.email : 'test@workeefy.com';
    this.userAddress = userData.basicUserInfo.addressInfo ? userData.basicUserInfo.addressInfo : '';
    this.userCard = userData.paymentInfo ? userData.paymentInfo : '';

  }

  nameEditHandler() {
    this.disableName ? this.disableName = false : this.disableName = true;
    this.nameGroup.setValue({ firstName: this.userData.basicUserInfo.firstName, lastName: this.userData.basicUserInfo.lastName });
  }

  updateName() {
    this.disableName = !this.disableName;
    let changedProperties = new UserDetailModel();
    changedProperties.BasicDetail.FirstName = this.nameGroup.controls.firstName.value;
    changedProperties.BasicDetail.LastName = this.nameGroup.controls.lastName.value;
    this.updateUser(changedProperties);
  }


  phoneEditHandler() {
    this.disablePhone ? this.disablePhone = false : this.disablePhone = true;
    this.phoneGroup.setValue({ phone: this.userData.basicUserInfo.phone })
  }
  updatePhone() {
    this.disablePhone = !this.disablePhone;
    let changedProperties = new UserDetailModel();
    changedProperties.BasicDetail.Phone = this.phoneGroup.controls.phone.value;
    this.updateUser(changedProperties);
  }


  showAddressModal() {
    let modal = this.modalCtrl.create(AutoCompleteModalPage);
    var currentRef = this;
    modal.onDidDismiss(data => {
      if (data != undefined) {
        this.userCompleteAddress.address = data;
        this.webSvc.fetchAddressDetail(data, this.addressObj, function (data) {
          currentRef.addNewAddress();
        });
      }
      else {
        console.log("address add cancelled");

      }
    })
    modal.present();
  }

  addNewAddress() {
    this.userCompleteAddress.city = this.addressObj.city;
    this.userCompleteAddress.latitude = this.addressObj.latitude;
    this.userCompleteAddress.longitude = this.addressObj.longitude;
    this.webSvc.addNewAddress(this.userCompleteAddress).subscribe(data => {
      this.userData = data;
      this.processUserData(data);
    })
  }

  openModal(value) {
    var currentRef = this;
    this.updatedItem = value ? 'card' : 'address';
    this.utils.showAlert('Add new ' + this.updatedItem, false, 'Are You really want to update your default ' + this.updatedItem, 'Yes', function () {
      currentRef.confirmChange();
    }, function () {
      currentRef.restoreDefaultCard();
    })
  }

  selectNewCard(index) {
    this.defaultCard = index;
    this.userData.paymentInfo.forEach((item, i) => {
      if (item.isDefault == true) {
        this.previousDefaultCard = i; //store previous default card if user does not confirm
      }
      if (index == i) {
        item.isDefault = true;
      } else {
        item.isDefault = false;
      }
    })
  }

  selectNewAddress(index) {
    this.defaultAddress = index;
    this.userAddress.forEach((item, i) => {
      if (item.isDefault == true) {
        this.previousDefaultAddress = i; //store previous default card if user does not confirm
      }
      if (index == i) {
        item.isDefault = true;
      } else {
        item.isDefault = false;
      }
    })
  }

  restoreDefaultCard() {
    if (this.updatedItem == 'card') {
      this.defaultCard = this.previousDefaultCard;
      this.userCard.forEach((item, i) => {
        if (this.previousDefaultCard == i) {
          item.isDefault = true;
        } else {
          item.isDefault = false;
        }
      })
    } else {
      this.defaultAddress = this.previousDefaultAddress;
      this.userAddress.forEach((item, i) => {
        if (this.previousDefaultAddress == i) {
          item.isDefault = true;
        } else {
          item.isDefault = false;
        }
      })
    }
  }

  confirmChange() {
    if (this.updatedItem == 'card') {
      this.userData.paymentInfo.forEach(card => {
        card.isDefault = false;
      });
      this.userCard[this.defaultCard].isDefault = true;
      this.webSvc.updateDefaultCard(this.userCard).subscribe(result => {
        this.processUserData(result);
        // this.saveUserData(result);
        this.userData = result;
        this.processUserData(this.userData);
      }, err => {
        console.log(err);
      })
    } else {
      this.userAddress.forEach(address => {
        address.isDefault = false;
      });
      this.userAddress[this.defaultAddress].isDefault = true;
      this.webSvc.setDefaultAddress(this.userAddress).subscribe(result => {
        this.processUserData(result);
        this.userData = result;
        this.processUserData(this.userData);
        // this.saveUserData(result);
      }, err => {
        console.log(err);
      })
    }

  }
  updateUser(changedProperties) {
    this.webSvc.updateUserProfile(changedProperties).subscribe(data => {
    }, err => {
      console.log(err);
    })
  }

  changePassword() {
    let modal = this.modalCtrl.create(ChangePasswordPage);
    modal.onDidDismiss(data => {

    })
    modal.present();
  }

  launchFileUpload() {
    var currentRef = this;
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Upload image using',
      buttons: [

        {
          text: 'Camera',
          handler: () => {
            currentRef.getImage(true);
          }
        },
        {
          text: 'Gallery',
          handler: () => {
            currentRef.getImage(false);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Action Cancelled');
          }
        }
      ]
    });

    actionSheet.present();
  }


  getImage(val) {
    var imgSource;
    var currentRef = this;
    if (val) {
      imgSource = this.camera.PictureSourceType.CAMERA;
    }
    else {
      imgSource = this.camera.PictureSourceType.PHOTOLIBRARY;
    }
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: imgSource
    }

    this.camera.getPicture(options).then((imageData) => {
      // currentRef.utils.showLoading("Uploading Image")
      this.imageURI = imageData;
      currentRef.webSvc.uploadFile(imageData).then((res) => {
        var obj = JSON.parse(res.response);
        currentRef.profilePicPath = obj.picUrl;
        currentRef.profilePic = currentRef.webSvc.API_profilePic_URL + obj.picUrl;
        let changedProperties = new UserDetailModel();
        changedProperties.BasicDetail.ProfilePic = obj.picUrl;
        this.updateUser(changedProperties);
      })
    }, (err) => {
      // currentRef.utils.showToaster("An error occured while uploading file");
      // currentRef.utils.hideLoading();

    });
  }

  addNewCard() {
    let modal = this.modalCtrl.create(AddNewCardPage, { userData: this.userData });
    modal.onDidDismiss(data => {
      if (data != undefined) {
        this.getUserData();
      }
      else{
        console.log("Modal closed");
      }
    })
    modal.present();
  }


}
