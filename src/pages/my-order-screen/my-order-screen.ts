import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Utils } from '../../providers/utils/utils';
import { WebService } from '../../providers/webService/webService';
import { SessionManagement } from '../../providers/sessionManagement/sessionManagement';
import { OrderDetailPage } from '../order-detail/order-detail';
@Component({
  selector: 'page-my-order-screen',
  templateUrl: 'my-order-screen.html',
})
export class MyOrderScreenPage {
  selectedIndex: any;
  pendingOrderList: any = [];
  closedOrderList: any = [];
  orderList: any = [{
    serviceSelected: "",
    displayName: "",
    charges: '',
    dateNdTime: ''
  }]
  selectedOrderList: any;
  selectedOrderTitle: string;
  constructor(public navCtrl: NavController, private utils: Utils, private webSvc: WebService, private sessionManagement: SessionManagement, ) {
    this.selectedOrderTitle = "pending";
    this.selectedIndex = 0;
  }
  ngOnInit() {
    this.getListData();
  }

  displayContent(index: any) {
    if (this.selectedIndex != index) {
      this.selectedIndex = index;
    }
    else {
      this.selectedIndex = -1;
    }
  }
  getListData() {
    this.utils.showLoading("Getting order list");
    var currentRef = this;
    this.webSvc.fetchUserService().subscribe(data => {
      currentRef.orderList = data;
      currentRef.selectOrderListData(currentRef.orderList);
    }, err => {
      currentRef.utils.showToaster("Something happend wrong")
    })
  }
  selectOrderListData(orderList) {
    this.closedOrderList = [];
    this.pendingOrderList = [];
    var currentRef = this;
    orderList.forEach(element => {
      switch (element.status.toLowerCase()) {
        case 'completed': {
          currentRef.closedOrderList.push(element);
          break;
        }
        case 'cancelled': {
          currentRef.closedOrderList.push(element);
          break;
        }
        default: {
          currentRef.pendingOrderList.push(element);
        }
      }
    });
    if (this.selectedOrderTitle == 'pending') {
      this.selectedOrderList = this.pendingOrderList;
    }
    else {
      this.selectedOrderList = this.closedOrderList;
    }
    this.utils.hideLoading();
  }

  cancelService(index) {
    var serviceId = this.selectedOrderList[index].serviceId
    this.utils.showLoading('Please wait');
    this.webSvc.cancelWorkeefyerService(serviceId).subscribe(result => {
      this.utils.hideLoading();
      this.utils.showToaster('Service has been cancelled');
      this.getListData(); 
      
    }, err => {
      this.utils.hideLoading();
    })
  }

  openDetailModal(index) {
    var serviceDetail: any;
    serviceDetail = this.selectedOrderList[index];
    this.navCtrl.push(OrderDetailPage, { serviceDetail: serviceDetail })
  }
}
