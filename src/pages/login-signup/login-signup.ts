import { Component, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { NavController, ModalController, ViewController, ActionSheetController, MenuController } from 'ionic-angular';

//import { AuthService, FacebookLoginProvider, GoogleLoginProvider } from "angular4-social-login";
// import { GooglePlus } from '@ionic-native/google-plus';
// import { Camera, CameraOptions } from '@ionic-native/camera';
import { Utils } from '../../providers/utils/utils';
import { SessionManagement } from '../../providers/sessionManagement/sessionManagement';
import { WebService } from '../../providers/webService/webService';
import { UserDetailModel } from '../../models/user-detail-model';
import { PaymentDetail } from '../../models/payment-detail';
import { Sidemenu } from '../sidemenu/sidemenu';
import { AutoCompleteModalPage } from '../auto-complete-modal/auto-complete-modal';
declare var Stripe: any;

@Component({
  selector: 'page-login-signup',
  templateUrl: 'login-signup.html',
})
export class LoginSignupPage {
  userObj: UserDetailModel;
  displayError: any;
  paymentFormInvalid: boolean = true;
  @ViewChild('companyInput') companyInput;
  @ViewChild('cardNumber') cardNumber;
  uploadedFilePath: string = "";
  isBasicDetailAccordianOpen: boolean = true;
  isCompanyDetailAccordianOpen: boolean = false;
  displayHeader: boolean = false;
  profilePicturePath: any;
  addressBarSetting;
  autocompleteItems;
  addressObj = { latitude: 0, city: "", longitude: 0 };
  address;
  userCity;
  userLatLong;
  userCompleteAddress;
  scrollTo: number;
  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};
  options = ['availabilityWeekend', 'availabilityAnytime', 'availabilityDayTimeWeekday'];
  private formContainer: any;
  private loginForm: FormGroup;
  private signupForm: FormGroup;
  imageURI: any;
  imageFileName: any;
  stripe;
  elements;
  cardNum;
  constructor(public navCtrl: NavController, private viewCtrl: ViewController, private utils: Utils, private modalCtrl: ModalController, public actionSheetCtrl: ActionSheetController, public menuCtrl: MenuController, public webSvc: WebService, public sessionManagement: SessionManagement) {
    this.formContainer = 'login';
    this.addressBarSetting = {
      showCurrentLocation: false,
      showRecentSearch: false,
      showSearchButton: false,
      geoCountryRestriction: "CAN",
      resOnSearchButtonClickOnly: false,
      inputPlaceholderText: 'Address',
    };

    this.autocompleteItems = [];
    this.address = {
      place: ''
    };

  }

  //Event propogate when Address selected by End User
  updateAddress(selectedData: any) {
    if (selectedData) {
      var vicinity = selectedData.vicinity ? selectedData.vicinity : selectedData.name;
      this.userLatLong = selectedData.geometry.location;
      if (vicinity.includes(",")) {
        this.userCity = vicinity.split(",")[0];
      }
      else {
        this.userCity = vicinity
      }
      this.userCompleteAddress = selectedData.description;
    }
    else {
      this.userCity = this.userCompleteAddress = "";
    }
  }

  ngOnInit() {
    if (this.formContainer == 'login') {
      this.createLoginForm();
    }

  }

  //function called when ever segment is changed
  segmentChanged(event) {
    if (event.value == "signup") {
      if (!this.signupForm) {
        this.createSignUpForm();
      } else {
        // this.signupForm.reset();
      }

    } else {
      if (!this.loginForm) {
        this.createLoginForm();
      } else {
        // this.loginForm.reset();
      }
    }
  }

  //function to create login form
  createLoginForm() {
    this.loginForm = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.pattern(this.utils.emailRegx)]),
      password: new FormControl(null, [Validators.required, Validators.minLength(8)])
    });
  }

  //function to create sign up form
  createSignUpForm() {
    this.signupForm = new FormGroup({
      firstName: new FormControl(null, [Validators.required, Validators.pattern(this.utils.alphaRegex)]),
      lastName: new FormControl(null, [Validators.required, Validators.pattern(this.utils.alphaRegex)]),
      address: new FormControl(null),
      phoneNumber: new FormControl(null, [Validators.required, Validators.maxLength(10), Validators.minLength(10)]),
      termsNConditionChecked: new FormControl(),
      email: new FormControl(null, [Validators.required, Validators.pattern(this.utils.emailRegx)]),
      password: new FormControl(null, [Validators.required, Validators.minLength(8)])
    });
  }

  configureStripe() {
    //Configure Stripe element for Payment
    this.stripe = Stripe('pk_test_kwoKDmMdx4b0wZKJSxcGgCns');
    this.elements = this.stripe.elements();
    var style = {
      iconStyle: 'solid',
      base: {
        color: '#32325d',
        lineHeight: '24px',
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSmoothing: 'antialiased',
        fontSize: '14px',
        '::placeholder': {
          color: '#aab7c4'
        }
      },
      invalid: {
        iconColor: '#fa755a'
      }
    };
    this.cardNum = this.elements.create('card', { hidePostalCode: true, style: style });
    this.cardNum.mount(this.cardNumber.nativeElement);
    var currentRef = this;
    //Handle real-time validation errors from the card Element.
    this.cardNum.addEventListener('change', function (event) { currentRef.cardError(event, 'cardNum-error'); });
  }

  cardError(event, elementName) {
    if (event.error) {
      this.paymentFormInvalid = true
      this.displayError = event.error.message;
    } else {
      this.paymentFormInvalid = false
      this.displayError = '';
    }
  }

  markDirty() {
    this.signupForm.controls.selectedService.markAsDirty();
  }

  showAddressModal() {
    let modal = this.modalCtrl.create(AutoCompleteModalPage, { searchedData: this.address.place });
    modal.onDidDismiss(data => {
      if (data != undefined) {
        this.address.place = data;
        this.webSvc.fetchAddressDetail(data, this.addressObj);
      }
      else{
        console.log("add address cancelled");
        
      }
    });
    modal.present();
  }

  presentForgetPasswordModal() {
    // let forgetPwdModal = this.modalCtrl.create(ForgotPasswordModal);
    // forgetPwdModal.onDidDismiss(data => {
    //   if (data) {
    //     this.utils.showBasicAlert("Reset Password Sent", "An password reset link had been email to " + data + "")
    //   }
    // });

    // forgetPwdModal.present();
  }

  socialLogin(provider: string) {
    //var currentRef = this;
    // this.utils.showLoading("Loading");
    if (provider === "fb") {
      this.utils.showToaster("Features coming soon");
      // this.utils.hideLoading();
      // this.authService.signIn(FacebookLoginProvider.PROVIDER_ID).then(result => {
      //   currentRef.checkUserExists(result.email, true);
      // }).catch(err => {
      //   this.utils.hideLoading();
      //   this.utils.processError(err)
      // });
    }
    else if (provider === "google") {
      this.utils.showToaster("Features coming soon");
      // this.utils.hideLoading();
      // this.googlePlus.login({
      //   'scopes': '', // optional, space-separated list of scopes, If not included or empty, defaults to `profile` and `email`.
      //   'webClientId': '934963175357-3a9tr063643kjo309pe9f3sdispkebdq.apps.googleusercontent.com', // optional clientId of your Web application from Credentials settings of your project - On Android, this MUST be included to get an idToken. On iOS, it is not required.
      //   'offline': true
      // }).then(result => {
      //   currentRef.checkUserExists(result.email, true);
      // }).catch(err => {
      //   this.utils.hideLoading();
      //   this.utils.processError(err)
      // });
    }
  }

  //Method invoke to Start Login process
  login() {
    // this.navCtrl.setRoot(Sidemenu)
    // this.utils.showLoading("Loading");
    this.webSvc.checkIfUserExist(this.loginForm.value.email).subscribe((result) => {
      if (result.val) {

        this.webSvc.loginUser({ "email": this.loginForm.value.email, "password": this.utils.encryptPassword(this.loginForm.value.password) }).subscribe(res => {
          // console.log(res);
          this.processUserDetail(res, res.token);
        }, err => {
          // this.utils.hideLoading();
          this.utils.processError(err);
        })
      }
      else {
        // this.utils.hideLoading();
        this.utils.showToaster("User doesn't exist");
      }
    })
  }

  openExternal(URL) {
    window.open(URL, '_system');
  }


  //Method after processing the Logged User Detail
  processUserDetail(result, token?) {
    if (result.userData.basicUserInfo.verified) {
      this.menuCtrl.enable(true);
      this.utils.orderedServices = result.userServices;
      this.utils._currentUser = result.userData;
      this.utils.showToaster("Welcome " + result.userData.basicUserInfo.firstName + " " + result.userData.basicUserInfo.lastName);

      this.sessionManagement.saveToken(token);
      this.utils.userToken = token;
      if ((result.userData.basicUserInfo.pushNotificationToken == "" || result.userData.basicUserInfo.pushNotificationToken != this.utils.pushNotification) && this.utils.isRealDevice()) {
        this.webSvc.updatePushId().subscribe(result => {
          this.utils._currentUser.userData = result;
          this.viewCtrl._nav.setRoot(Sidemenu);
          // this.makeOnline();
        }, err => {
          console.log("An error occured")
        });
      }
      else {
        this.viewCtrl._nav.setRoot(Sidemenu);
      }
    } else {
      //show error that verification is pending
    }

  }

  basicDetailAccordianHandler() {
    if (!this.isBasicDetailAccordianOpen) {
      this.isBasicDetailAccordianOpen = true;
    }
    else
      this.isBasicDetailAccordianOpen = false;
  }
  companyDetailAccordianHandler() {
    if (!this.isCompanyDetailAccordianOpen) {
      this.isCompanyDetailAccordianOpen = true;
      this.configureStripe();
    }
    else
      this.isCompanyDetailAccordianOpen = false;
  }

  //Method invoke to Start Signup process
  signUp() {
    this.webSvc.checkIfUserExist(this.signupForm.value.email).subscribe((result) => {
      if (result.val) {
        this.utils.showToaster("Email Already Register, Please use a new Email")
      }
      else {
        //Navigate to Check for workeefyer
        this.proceedWithSignUp()
      }
    }, err => {
      this.utils.processError(err.message);
    })
  }

  proceedWithSignUp() {
    this.userObj = new UserDetailModel();
    this.userObj.BasicDetail.FirstName = this.signupForm.value.firstName;
    this.userObj.BasicDetail.LastName = this.signupForm.value.lastName;
    this.userObj.BasicDetail.Email = this.signupForm.value.email;
    this.userObj.BasicDetail.Password = this.utils.encryptPassword(this.signupForm.value.password);
    this.userObj.BasicDetail.Phone = this.signupForm.value.phoneNumber;
    this.userObj.BasicDetail.Address = this.signupForm.value.address;
    this.userObj.BasicDetail.PostalCode = this.signupForm.value.postalCode;
    this.userObj.BasicDetail.ProfilePic = this.uploadedFilePath;
    this.userObj.BasicDetail.City = this.addressObj.city;
    this.userObj.BasicDetail.Latitude = this.addressObj.latitude;
    this.userObj.BasicDetail.Longitude = this.addressObj.longitude;
    this.userObj.BasicDetail.ForSocial = false;
    let extraDetails = {
      owner: {
        name: this.userObj.BasicDetail.FirstName + ' ' + this.userObj.BasicDetail.LastName,
        email: this.userObj.BasicDetail.Email
      },
      usage: "reusable"
    }
    var currentRef = this;
    currentRef.stripe.createSource(currentRef.cardNum, extraDetails).then(function (result) {
      if (result.error) {
        // var errorElement = document.getElementById('card-errors');
        currentRef.utils.showToaster(result.error.message);
      } else {
        let paymentobj = new PaymentDetail();
        paymentobj.SourceId = result.source.id;
        paymentobj.Card4Digits = result.source.card.last4;
        paymentobj.CardType = result.source.card.brand;
        currentRef.stripe.createToken(currentRef.cardNum).then(function (res) {
          paymentobj.TokenId = res.token.id;
          currentRef.userObj.PaymentDetail.push(paymentobj);
          currentRef.userObj.PaymentDetail.splice(0, 1);
          currentRef.saveUserDetail();
        })
      }
    });


  }
  saveUserDetail() {
    var currentRef = this;
    this.webSvc.registerUser(this.userObj).subscribe(res => {
      this.utils.showBasicAlert("Thank you for registeration", "An account activation email had been sent to " + this.userObj.BasicDetail.Email + " " + 'Please verify within 24 hours', function () {
        currentRef.formContainer = 'login';
        currentRef.signupForm.reset();
      });
    }, err => {
      this.utils.showToaster(err.message);
    });
  }


  //Method to check if User Email already register in system
  checkUserExists(email, forSocial?) {
    var currentRef = this;
    this.webSvc.checkIfUserExist(email, forSocial).subscribe((result) => {
      if (result.val) {
        // console.log(result.token, result.user, result.userId)
        currentRef.fetchUserDetail(result)
      }
      else {
        this.utils.showToaster("User does not exist")
        // this.utils.hideLoading();
        //this.utils.partialRegistration.email = email;
        //this.chooseUserType.open();
        //Navigate to Check for workeefyer
      }
    }, err => {
      // this.utils.hideLoading();
      this.utils.processError(err);
    })
  }

  fetchUserDetail(userData) {
    // this.webSvc.fetchUserInfo(userData.userId, userData.token).subscribe((result) => {
    //   this.processUserDetail(result.userData, userData.token);
    // }, err => {
    //   this.utils.hideLoading();
    //   this.utils.processError(err);
    // })
  }
  goToSignup() {
    this.formContainer = 'signup';
  }
  goToLogin() {
    this.formContainer = 'login';
  }

}
