import { Component, ViewChild } from '@angular/core';
import { NavParams, ModalController, Slides } from 'ionic-angular';
import { WebService } from './../../providers/webService/webService';
import { Utils } from './../../providers/utils/utils';
import { SessionManagement } from './../../providers/sessionManagement/sessionManagement'
import { ChangeAddressModalPage } from './../change-address-modal/change-address-modal';
// import {AutoCompleteModalPage} from '../auto-complete-modal/auto-complete-modal';

@Component({
  selector: 'page-service-select',
  templateUrl: 'service-select.html',
})
export class ServiceSelectPage {
  @ViewChild(Slides) slides: Slides;
  workeefyerList: any = [];
  selectedAddressInfo: any;
  description: string;
  userData: any;
  activeServices: any[];
  selectedService: any;
  selectedCity: any;
  availServiceTime: any;
  scheduleTime: any;
  selectedAddress: any = "";
  startDate = new Date();
  scheduleDate = new Date();
  isScheduleDate: boolean = false;
  endAfterArray: any = [];
  serviceSelectOption: any = {
    title: "Select service",
  }
  scheduleOption: any = {
    title: "Schedule Service"
  }
  constructor(private utils: Utils, private params: NavParams, private webSvc: WebService, private modalCtrl: ModalController, private sessionManagement: SessionManagement, ) {
    this.availServiceTime = "Now";
    var serviceParam=this.params.get("serviceName");
    if(serviceParam){
      this.selectedService = serviceParam;
    }
    else{
      this.selectedService="HomeClean"
    }
    this.endAfterArray.length = 10;
  }
  ngOnInit() {
    this.getUserData();
    this.utils.activeServices.length > 0 ? this.activeServices = this.utils.activeServices : this.webSvc.getAllActiveService().subscribe(result => {
      if (result) {
        this.activeServices = result.activeServices;
      }
    }, err => {
      this.utils.showToaster("No Internet");
      console.log(err);
    })
  }
  getUserData() {
    this.sessionManagement.getToken().then(token => {
      if (token) {
        let uuid = this.utils.decodeToken(token).userId;
        this.webSvc.fetchUserInfo(uuid).subscribe(result => {
          // this.utils._currentUser.userData = result;
          this.userData = result.userData.userData;
          this.userData.basicUserInfo.addressInfo.forEach(element => {
            if (element.isDefault) {
              this.selectedAddress = element.address;
              this.selectedCity = element.city;
              this.selectedAddressInfo = element;
              this.fetchWorkeefyerList(this.selectedService, this.selectedCity, this.selectedAddress);
            }
          });
          // this.processUserData(this.userData);
        }, err => {
          console.log("An error occured")
        });
      } else {
        console.log("An error occured")
      }
    });

  }

  fetchWorkeefyerList(serviceName?, cityName?, address?) {
    this.webSvc.fetchCityWiseWorkeefye(serviceName, cityName, address).subscribe((result) => {
      if (result.value.length > 0)
        this.workeefyerList = result.value;
      else this.workeefyerList.length = 0;
    },
      err => {
        this.utils.showToaster("Something went wrong !! Check your internet or try again later")
      })
  }

  nextWorkeefyer() {
    this.slides.slideNext();

  }
  prevWorkeefyer() {
    this.slides.slidePrev();
    // console.log('Current index is', currentIndex);
  }

  defaultServiceChange(value) {
    this.selectedService = value;
    this.fetchWorkeefyerList(this.selectedService, this.selectedCity, this.selectedAddress);
  }

  setScheduleSet(event) {
    console.log(event);
    this.scheduleDate = event;
    this.isScheduleDate = true;
    // this.scheduleDate=
  }
  showAddressModal() {
    let modal = this.modalCtrl.create(ChangeAddressModalPage, { userAddressData: this.userData.basicUserInfo.addressInfo, selectedAddress: this.selectedAddress });
    modal.onDidDismiss(data => {
      this.selectedAddress = data.address;
      this.selectedCity = data.city;
      this.selectedAddressInfo = data;
      this.fetchWorkeefyerList(this.selectedService, this.selectedCity, this.selectedAddress);
    })
    modal.present();
  }
  resetScheduleObj() {
    this.utils.resetSchedule();
  }
  selectTime(value) {
    this.utils.scheduleObj.time = value;
  }
  updateCheckedOptions(value) {
    this.utils.scheduleObj.recurrence = value;
  }
  setOccurence(value) {
    this.utils.scheduleObj.occurrence = value;
  }
  placeOrder(value) {
    this.utils.scheduleObj.type = value;
    var currentRef = this;
    this.getServiceNameByDisplayName(this.selectedService, function (serviceName) {
      var message = {
        content: 'Are you sure you want to order a workeefyer for',
        text: serviceName,
      }
      currentRef.utils.showAlert('Service Select ', false, message, 'Yes', function () {
        // currentRef.confirmChange();
        currentRef.confirmWorkeefyer();
      }, function () {
        // currentRef.restoreDefaultCard();
      })
    });
  }

  getServiceNameByDisplayName(value, next) {
    this.activeServices.forEach(option => {
      if (option.serviceName == value)
        next(option.displayName);
    })
  }

  confirmWorkeefyer() {

    let scheduleDetails = {
      date: new Date(),
      description: '',
      serviceName: '',
      servicingCity: '',
      laterSchedule: false,
      addressInfo: {

      },
      scheduleSlotInfo: {
        forMorning: false,
        forAfternoon: false,
        forEvening: false,
        forFullDay: false,
        recurrence: false,
        forMonthly: false,
        forWeekly: false,
        occuranceCount: 0
      }
    }
    scheduleDetails.date = this.utils.scheduleObj.date;
    scheduleDetails.addressInfo = this.selectedAddressInfo;//utils._currentUser.basicUserInfo.addressInfo[0];
    scheduleDetails.serviceName = this.selectedService;
    scheduleDetails.servicingCity = this.selectedAddressInfo.city;
    scheduleDetails.description = this.description;
    switch (this.utils.scheduleObj.type) {
      case 0: this.utils.showToaster('Please fill all the required details...');
        break;
      case 1: this.utils.scheduleObj.date = new Date();//Date.now();
        break;
      case 2:
        this.utils.processServiceTime(scheduleDetails);
        scheduleDetails.laterSchedule = true;
        if (this.utils.scheduleObj.recurrence) {
          scheduleDetails.scheduleSlotInfo.recurrence = true;
          this.utils.processReccurence(scheduleDetails);
          scheduleDetails.scheduleSlotInfo.occuranceCount = this.utils.scheduleObj.occurrence ? this.utils.scheduleObj.occurrence : 0;
        } else {
          scheduleDetails.scheduleSlotInfo.recurrence = false;
        }
        break;
      default: break;
    }
    // this.utils.showLoader();
    this.webSvc.makeServiceRequest(scheduleDetails).subscribe(result => {
      // this.utils.hideLoader();
      // this.showConfimation = true;
    }, err => {
      this.utils.showToaster('An Error has occurred');
      // this.utils.hideLoader();
    })
    console.log(scheduleDetails);
  }

}
