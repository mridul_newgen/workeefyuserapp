import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, ModalController } from 'ionic-angular';

import { Utils } from './../../providers/utils/utils';
import { WebService } from './../../providers/webService/webService';
import { AutoCompleteModalPage } from '../auto-complete-modal/auto-complete-modal';
@Component({
  selector: 'page-change-address-modal',
  templateUrl: 'change-address-modal.html',
})
export class ChangeAddressModalPage {
  selectedAddress: any;
  selectedAddressInfo: any
  previousSelectedAddressInfo: any
  userAddress: any = [];
  userCompleteAddress = {
    address: '',
    city: '',
    latitude: 0,
    longitude: 0,
    isDefault: false
  }
  constructor(public navCtrl: NavController, private modalCtrl: ModalController, public viewCtrl: ViewController, public navParams: NavParams, private utils: Utils, private webSvc: WebService) {
    this.userAddress = this.navParams.get('userAddressData');
    this.selectedAddress = this.navParams.get('selectedAddress');
    this.userAddress.forEach(element => {
      if (element.address == this.selectedAddress)
        this.selectedAddressInfo = element;
    });
  }
  closeModal() {
    this.viewCtrl.dismiss(this.selectedAddressInfo);
  }

  selectNewAddress(index) {
    var currentRef = this;
    this.userAddress.forEach((element, i) => {
      if (index == i) {
        currentRef.previousSelectedAddressInfo=currentRef.selectedAddressInfo;
        this.selectedAddress=element.address;
        currentRef.selectedAddressInfo=element;
      }
    });
  }

  openModal(value) {
    var currentRef = this;
    this.utils.showAlert('Select new address', false, 'Are You really want to select this address', 'Yes', function () {
      currentRef.viewCtrl.dismiss(currentRef.selectedAddressInfo);
    }, function () {
      // currentRef.viewCtrl.dismiss(currentRef.restoreDefaultAddress());
      currentRef.restoreDefaultAddress();
    })
  }

  addAddress() {
    let modal = this.modalCtrl.create(AutoCompleteModalPage);
    var currentRef = this;
    modal.onDidDismiss(data => {
      if (data != undefined) {
        this.userCompleteAddress.address = data;
        this.webSvc.fetchAddressDetail(data, this.userCompleteAddress, function (data) {
          currentRef.addNewAddress();
        });
      }
      else {
        console.log("address add cancelled");

      }
    })
    modal.present();
  }

  addNewAddress() {
    // this.userCompleteAddress.city = this.userCompleteAddress.city;
    // this.userCompleteAddress.latitude = this.userCompleteAddress.latitude;
    // this.userCompleteAddress.longitude = this.userCompleteAddress.longitude;
    this.webSvc.addNewAddress(this.userCompleteAddress).subscribe(data => {
      this.userAddress = data.basicUserInfo.addressInfo;;
      // this.processUserData(data);
    })
  }

  restoreDefaultAddress() {
    this.selectedAddressInfo=this.previousSelectedAddressInfo;
    this.selectedAddress=this.previousSelectedAddressInfo.address;
    return  this.selectedAddressInfo;
  }

}
