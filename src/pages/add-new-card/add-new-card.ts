import { Component, ViewChild, } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { Utils } from '../../providers/utils/utils';
import { WebService } from '../../providers/webService/webService';
declare var Stripe: any;

@Component({
  selector: 'page-add-new-card',
  templateUrl: 'add-new-card.html',
})
export class AddNewCardPage {
  paymentsArray: any = [];
  displayError: any;
  paymentFormInvalid: boolean;
  cardNum: any;
  elements: any;
  stripe: any;
  profile: any = [];
  @ViewChild('cardNumber') cardNumber;
  constructor(public viewCtrl: ViewController, private utils: Utils, private webSvc: WebService, private params: NavParams) {
    this.profile = this.params.get('userData');
    this.paymentsArray = this.profile.paymentInfo;
  }
  ngOnInit() {
    this.configureStripe();
  }
  closeModal() {
    this.viewCtrl.dismiss();
  }
  configureStripe() {
    //Configure Stripe element for Payment
    this.stripe = Stripe('pk_test_kwoKDmMdx4b0wZKJSxcGgCns');
    this.elements = this.stripe.elements();
    var style = {
      iconStyle: 'solid',
      base: {
        color: '#32325d',
        lineHeight: '24px',
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSmoothing: 'antialiased',
        fontSize: '14px',
        '::placeholder': {
          color: '#aab7c4'
        }
      },
      invalid: {
        iconColor: '#fa755a'
      }
    };
    this.cardNum = this.elements.create('card', { hidePostalCode: true, style: style });
    this.cardNum.mount(this.cardNumber.nativeElement);
    var currentRef = this;
    //Handle real-time validation errors from the card Element.
    this.cardNum.addEventListener('change', function (event) { currentRef.cardError(event, 'cardNum-error'); });
  }

  cardError(event, elementName) {
    if (event.error) {
      this.paymentFormInvalid = true
      this.displayError = event.error.message;
    } else {
      this.paymentFormInvalid = false
      this.displayError = '';
    }
  }

  updateCard() {
    // this.utils.showLoader();
    let data = {
      sourceId: 0,
      tokenId: 0,
      card4Digits: 0,
      cardType: '',
      customerId: 0
    }
    var currentRef = this;
    let extraDetails = {
      owner: {
        name: this.profile.basicUserInfo.firstName ? this.profile.basicUserInfo.firstName : '' + ' ' + this.profile.basicUserInfo.lastName ? this.profile.basicUserInfo.lastName : '',
        email: this.profile.basicUserInfo.email ? this.profile.basicUserInfo.email : ''
      },
      usage: "reusable"
    }
    currentRef.stripe.createSource(currentRef.cardNum, extraDetails).then(function (result) {
      if (result.error) {
        // currentRef.utils.hideLoader();
        // var errorElement = document.getElementById('card-errors');
        // currentRef.toastr.error(result.error.message);
      } else {
        data.sourceId = result.source.id;
        data.card4Digits = result.source.card.last4;
        data.cardType = result.source.card.brand;
        data.customerId = currentRef.paymentsArray.length > 0 ? currentRef.paymentsArray[0].customerId : 0;
        currentRef.stripe.createToken(currentRef.cardNum).then(function (res) {
          data.tokenId = res.token.id;
          let userData = {
            basicUserInfo: {},
            paymentDetail: {}
          }
          userData.basicUserInfo = currentRef.profile.basicUserInfo;
          userData.paymentDetail = data;
          currentRef.updatecardInfo(userData);
        })

      }
    });
  }

  updatecardInfo(data) {
    this.webSvc.addNewCard(data).subscribe(result => {
      // this.utils.hideLoader();
      this.viewCtrl.dismiss();
    }, err => {
      // this.utils.hideLoader();
      var err = JSON.parse(err._body).message;
      this.utils.showToaster(err);
      console.log(err);
    })
  }


}
