import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ServiceOrderConfirmationPage } from './service-order-confirmation';

@NgModule({
  declarations: [
    ServiceOrderConfirmationPage,
  ],
  imports: [
    IonicPageModule.forChild(ServiceOrderConfirmationPage),
  ],
})
export class ServiceOrderConfirmationPageModule {}
