import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';
import { WebService } from './../../providers/webService/webService';
import { Utils } from './../../providers/utils/utils';

@Component({
  selector: 'page-order-detail',
  templateUrl: 'order-detail.html',
})
export class OrderDetailPage {
  serviceDetail: any;
  serviceDetailToShow = {
    serviceSelected: '',
    serviceDisplayName: '',
    serviceStartTime: '',
    profilePic: '',
    workeefyerName: '',
    status: '',
    otp: '',
    totalAmountPaid: '',
    totalHours: '',
    workeefyerPhone:'',
    serviceAddress:''
  };
  activeService: any = {};

  serviceIcon: any;
  constructor(public navParams: NavParams, private webSvc: WebService) {
    this.serviceDetail = this.navParams.get('serviceDetail');
    this.serviceIcon = './assets/icon/' + this.serviceDetail.serviceSelected + '.svg';
    this.serviceDetailToShow.profilePic = this.serviceDetail && this.serviceDetail.workeefy ? this.webSvc.API_URL + this.serviceDetail.workeefy.basicUserInfo.profilePic : './assets/images/noProfile.png';
    this.serviceDetailToShow.status = this.serviceDetail ? this.serviceDetail.status : undefined;
    this.serviceDetailToShow.totalAmountPaid = this.serviceDetail && this.serviceDetail.isAmountOverride ? this.serviceDetail.overrideAmount : this.serviceDetail && this.serviceDetail.finalServiceCharge ? this.serviceDetail.finalServiceCharge : 0;
    this.serviceDetailToShow.totalHours = this.serviceDetail && this.serviceDetail.duration ? this.serviceDetail.duration : 0;
    this.serviceDetailToShow.serviceDisplayName = this.serviceDetail.serviceDisplayName;
    this.serviceDetailToShow.serviceSelected = this.serviceDetail.serviceSelected;
    this.serviceDetailToShow.serviceStartTime = this.serviceDetail.serviceRequestTime;
    this.serviceDetailToShow.otp = this.serviceDetail.verifyToken;
    this.serviceDetailToShow.serviceAddress = this.serviceDetail.serviceAddress.address;
    this.serviceDetailToShow.workeefyerName = this.serviceDetail && this.serviceDetail.workeefy ? this.serviceDetail.workeefy.basicUserInfo.firstName + ' ' + this.serviceDetail.workeefy.basicUserInfo.lastName : 'test';
    this.serviceDetailToShow.workeefyerPhone = this.serviceDetail && this.serviceDetail.workeefy ?this.serviceDetail.workeefy.basicUserInfo.phone : '123456789';
    // this.utils.activeServices.forEach(element => {
    //   if(element.serviceName == this.serviceDetailToShow.serviceDisplayName){
    //     this.activeService=element;
    //   }
    // });
    this.webSvc.getAllActiveService().subscribe(result => {
      if (result) {
        result.activeServices.forEach(element => {
          if (element.displayName == this.serviceDetailToShow.serviceDisplayName) {
            this.activeService = element;
            // console.log(this.activeService);

          }
        });
      }
    })
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad OrderDetailPage');
  }

}
