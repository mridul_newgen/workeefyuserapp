import { Injectable } from '@angular/core';

import { Storage } from '@ionic/storage';

// import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class SessionManagement {
    constructor(private storage: Storage) {

    }

    //Save the Token in device app memory
    saveToken(token) {
        this.storage.ready().then(() => {
            this.storage.set('token', token);
        });
    }

    //get the Token Saved in App Session Memory
    getToken() {
        return this.storage.ready().then(() => {
            return this.storage.get('token');
        });
    }
    
    //Save the started service ID in device app memory
    saveServiceId(serviceId) {
        this.storage.ready().then(() => {
            this.storage.set('serviceId', serviceId);
        });
    }

    //get the started service ID in device app memory
    getServiceId() {
        return this.storage.ready().then(() => {
            return this.storage.get('serviceId');
        });
    }

    deleteSession() {
        return this.storage.ready().then(() => {
            return this.storage.clear();
        });
    }


    //Save User 
}