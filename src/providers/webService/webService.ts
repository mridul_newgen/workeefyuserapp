import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { FileTransfer, FileTransferObject, FileUploadOptions } from '@ionic-native/file-transfer';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Utils } from '../utils/utils';
import { SessionManagement } from '../sessionManagement/sessionManagement'

declare var google: any;

@Injectable()
export class WebService {
  public API_profilePic_URL = "http://workeefy.eastus.cloudapp.azure.com:8877/";
  public API_URL = "http://192.168.22.32:8877";
  // public API_URL = "http://workeefy.eastus.cloudapp.azure.com:8877";
  constructor(public http: Http, public utils: Utils, public sessionManagement: SessionManagement, private transfer: FileTransfer) {
  }

  //get all active services
  getAllActiveService(deviceToken?) {
    return this.http.get(this.API_URL + '/auth/getAllActiveService')
      .map(res => res.json())
      .catch((error: any) =>
        Observable.throw(error || 'Server error'))
  }

  //Check if User exist in System
  checkIfUserExist(email, forSocial?) {
    var forSocial = forSocial ? forSocial : false;
    return this.http.get(this.API_URL + '/auth/userExist?email=' + email + '&forSocial=' + forSocial)
      .map(res => res.json())
      .catch((error: any) =>
        Observable.throw(error || 'Server error'));
  }

  //Sign in User
  loginUser(userObj) {
    return this.http.post(this.API_URL + '/auth/signInEndUser', userObj)
      .map(res => res.json())
      .catch((error: any) =>
        Observable.throw(error || 'Server error'));
  }

  //Fetch End User Detail from System
  fetchUserInfo(uid) {
    return this.http.get(this.API_URL + '/services/fetchUserInfo')
      .map(res => res.json())
      .catch((error: any) =>
        Observable.throw(error || 'Server error'));
  }

  addNewAddress(addressDetail) {

    return this.http.post(this.API_URL + '/services/addMoreAddress', { addressDetail: addressDetail })
      .map(res => res.json())
      .catch((error: any) =>
        Observable.throw(error || 'Server error'))
  }

  addNewCard(cardDetails) {
    return this.http.post(this.API_URL + '/services/addMoreDetail', cardDetails)
      .map(res => res.json())
      .catch((error: any) =>
        Observable.throw(error || 'Server error'))
  }

  //update PushNotification token in User Account
  updatePushId() {
    let reqObj = {
      pushNotificationId: this.utils.pushNotification.registrationId
    }

    return this.http.post(this.API_URL + '/services/updatePushId', reqObj)
      .map(res => res.json())
      .catch((error: any) =>
        Observable.throw(error || 'Server error'));

  }


  //Upload Profile Pic at Server
  uploadFile(imageURI) {
    const fileTransfer: FileTransferObject = this.transfer.create();
    let options: FileUploadOptions = {
      fileKey: 'profilePic',
      headers: {}
    }

    return fileTransfer.upload(imageURI, this.API_URL + '/auth/savePhoto', options)
      .then((data) => {
        console.log(data + " Uploaded Successfully", data);
        return data;
      }, (err) => {
        console.log(err);
        return err;

      });
  }

  updatePassword(password) {
    let reqObj = {
      password: password
    }
    return this.http.post(this.API_URL + '/auth/changePassword', reqObj)
      .map(res => res.json())
      .catch((error: any) =>
        Observable.throw(error || 'Server error'));
  }

  updateUserProfile(userDetails) {
    let body = {
      userDetails: userDetails
    }

    return this.http.post(this.API_URL + '/services/updateUserProfile', body)
      .map(res => res.json())
      .catch((error: any) =>
        Observable.throw(error || 'Server error'))
  }

  //fetch city & lat and long
  fetchAddressDetail(address: any, addressDetail, next?) {
    let geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'address': address }, (results, status) => {
      addressDetail.latitude = results[0].geometry.location.lat(),
        addressDetail.longitude = results[0].geometry.location.lng(),
        addressDetail.city = this.utils.parse_place(results[0], 'locality')
      //alert("lat: " + this.latitude + ", long: " + this.longitude);
      if (next) {
        next(addressDetail);
      }
    });
  }
  //Register as a User
  registerUser(userObj) {
    return this.http.post(this.API_URL + '/auth/registerUser', userObj)
      .map(res => res.json())
      .catch((error: any) =>
        Observable.throw(error || 'Server error'));
  }

  updateDefaultCard(cardDetails, deviceToken?) {
    let body = {
      paymentDetail: cardDetails
    }
    return this.http.post(this.API_URL + '/services/updateDefaultCard', body)
      .map(res => res.json())
      .catch((error: any) =>
        Observable.throw(error || 'Server error'))
  }

  setDefaultAddress(addressDetail) {
    return this.http.post(this.API_URL + '/services/setDefaultAddress', { addressDetail: addressDetail })
      .map(res => res.json())
      .catch((error: any) =>
        Observable.throw(error || 'Server error'))
  }

  //Fetch Workeefyer List providing Selected Service City wise from System  
  fetchCityWiseWorkeefye(serviceType, cityName, address?) {
    var urlPath = this.API_URL + '/get/fetchCityWiseWorkeefye?serviceType=' + serviceType + '&city=' + cityName;
    if (address && address != "") {
      urlPath = urlPath + "&address=" + address;
    }
    return this.http.get(urlPath)
      .map(res => res.json())
      .catch((error: any) =>
        Observable.throw(error || 'Server error'));
  }

  //Fetch Scheduled Service for End User from System
  fetchUserService() {
    return this.http.get(this.API_URL + '/services/userServices')
      .map(res => res.json())
      .catch((error: any) =>
        Observable.throw(error || 'Server error'));
  }
  
  //Register for selected service to selected workeefyer in ServiceSchedule table
  makeServiceRequest(reqObj) {
    return this.http.post(this.API_URL + '/services/registerNew', reqObj)
      .map(res => res.json())
      .catch((error: any) =>
        Observable.throw(error || 'Server error'));

  }

   //Cancel ordered service for a workeefyer
   cancelWorkeefyerService(serviceId, deviceToken?) {
    let reqObj = {
      serviceId: serviceId
    }
    return this.http.post(this.API_URL + '/services/cancelService', reqObj)
      .map(res => res.json())
      .catch((error: any) =>
        Observable.throw(error || 'Server error'));
  }

}
