
// import { Http, RequestOptionsArgs, Response, RequestOptions, ConnectionBackend, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Utils } from '../utils/utils';
import { SessionManagement } from '../sessionManagement/sessionManagement';
// import { HttpInterceptor, HttpRequest } from '@angular/common/http/';
// import { HttpHandler } from '@angular/common/http';
import { ConnectionBackend, RequestOptions, RequestOptionsArgs, Http, Headers, Response } from '@angular/http';

export class HttpProvider extends Http {
    count = 0;
    constructor(connectionBackend: ConnectionBackend, requestOptions: RequestOptions, public utils: Utils, public sessionManagement: SessionManagement) {
        super(connectionBackend, requestOptions);
    }

    get(url: string, options?: RequestOptionsArgs): Observable<Response> {
        this.utils.showLoading('please wait');
        this.utils.httpRequestCount++;
        return Observable.fromPromise(this.getRequestOptionArgs()).mergeMap((options) => {
            this.utils.httpRequestCount--;
            this.utils.httpRequestCount == 0 ? this.utils.hideLoading() : null;
            return super.get(url, options)
        });
    }

    post(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
        this.utils.showLoading('please wait');
        this.utils.httpRequestCount++;
        return Observable.fromPromise(this.getRequestOptionArgs()).mergeMap((options) => {
            this.utils.httpRequestCount--;
            this.utils.httpRequestCount == 0 ? this.utils.hideLoading() : null;
            return super.post(url, body, options)
        })
    }

    put(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
        this.utils.showLoading('please wait');
        this.utils.httpRequestCount++;
        return Observable.fromPromise(this.getRequestOptionArgs()).mergeMap((options) => {
            this.utils.httpRequestCount--;
            this.utils.httpRequestCount == 0 ? this.utils.hideLoading() : null;
            return super.put(url, body, options)
        })
    }

    getRequestOptionArgs(options?: RequestOptionsArgs) {
        return this.sessionManagement.getToken().then((token) => {
            if (options == null) {
                options = new RequestOptions();
            }
            if (options.headers == null) {
                options.headers = new Headers();
            }
            if (token !== null) {
                options.headers.append('x-access-token', token);
            }
            options.headers.append('Content-Type', 'application/json');
            return options;
        })
    }
}


// import { Http, RequestOptionsArgs, Response, RequestOptions, ConnectionBackend, Headers } from '@angular/http';
// import 'rxjs/add/operator/map';
// import { Observable } from 'rxjs/Observable';
// import { Storage } from '@ionic/storage';
// import { Utils } from '../utils/utils';
// import { HttpInterceptor, HttpRequest } from '@angular/common/http/';
// import { HttpHandler } from '@angular/common/http';

// export class HttpProvider implements HttpInterceptor {
//     constructor(public storage: Storage, public utils: Utils) { }
//     intercept(req: HttpRequest<any>, next: HttpHandler) {
//         if (this.utils.userToken !== null) {
//             const newReq = req.clone({
//                 headers: req.headers.set(
//                     'Authorization',
//                     'token your-auth-token'
//                 )
//             });
//         }
//         const newReq = req.clone({
//             headers: req.headers.set(
//                 'Content-Type',
//                 'application/json'
//             )
//         });
//         return next.handle(newReq);
//     }

// }


