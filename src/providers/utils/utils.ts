import { Injectable } from '@angular/core';
import { LoadingController, AlertController, ToastController, Platform } from 'ionic-angular';
import * as CryptoJS from 'crypto-js';
import { JwtHelper } from 'angular2-jwt';



// import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class Utils {
  orderedServices: any;
  httpRequestCount = 0;
  public device: any;
  alphaRegex = /^[a-zA-Z]*$/;
  phoneRegex = /^[1-9]{1}[0-9]{9}$/;
  public emailRegx: any = /^(([^<>()[\]\\.,;:!#$%&*\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  public _currentUser = {
    userData:
      {
        basicUserInfo:
          {
            profilePic: "", firstName: "", lastName: "", email: "", address: "",
            pushNotificationToken: "", createdDate: ""
          },
        workeefyAvailability: { status: "" },
        workingServiceId: ""
      }
  }
  scheduleObj = {
    date: new Date(),
    type: 0,
    time: 0,
    recurrence: 0,
    occurrence: 0,
    description: ''
  };
  scheduleTime = ['NA', 'Morning', 'Afternoon', 'Evening', 'Full Day'];
  scheduleTypes = ['NA', 'now', 'scheduled', 'occurring'];
  scheduleRecurrence = ['NA', 'weekly', 'monthly'];
  public status: string = "Offline";
  public profilePic = "./assets/images/noProfile.png";
  public defaultProfilePic = "./assets/images/noProfile.png";
  public isAvailable: boolean = false;
  private loader: any;
  private toaster: any;
  private alertMessage: string;
  public userToken: string = "";
  public pushNotification: any;
  private isSpinnerShown: boolean;
  private jwtHelper: JwtHelper;
  public acceptedServiceId: string;
  public currentPendingRequestData: any;
  public pendingServiceNotification: number;
  public activeServices = [];
  constructor(private alertCtrl: AlertController, private toastCtrl: ToastController, private loadingCtrl: LoadingController, private platform: Platform) {
    this.pendingServiceNotification = 0;
    this.acceptedServiceId = "";
    this.jwtHelper = new JwtHelper();
    if (this.platform.is('ios')) {
      this.device = 'ios';
    }
    if (this.platform.is('android')) {
      this.device = 'android';
    }
  }

  public resetUserData() {
    this._currentUser = {
      userData:
        {
          basicUserInfo:
            {
              profilePic: "", firstName: "", lastName: "", email: "", address: "",
              pushNotificationToken: "", createdDate: ""
            },
          workeefyAvailability: { status: "" },
          workingServiceId: ""
        }
    }
  }

  public clearCurrentServiceData() {
    this.currentPendingRequestData = {};
    this.acceptedServiceId = "";
    this.resetUserData();
  }


  //Encrypt Password before sending to Server API
  public encryptPassword(password) {
    var key = CryptoJS.enc.Base64.parse("WorkeefyerSecretKey");
    var iv = CryptoJS.enc.Base64.parse("#base64IV#");
    var encryptedPassword = CryptoJS.AES.encrypt(password, key, { iv: iv });
    return encryptedPassword.toString();
  }

  //Utils for Displaying the Loading Control across the Application
  showLoading(message) {
    if (!this.isSpinnerShown) {
      this.loader = this.loadingCtrl.create({
        content: message,
        spinner: 'crescent'
        // dismissOnPageChange: true
      });
      this.loader.present();
      this.isSpinnerShown = true;
    }
  }

  //Utils for Hide the Loading Control across the Application
  hideLoading() {
    if (this.isSpinnerShown) {
      this.loader.dismiss();
      this.isSpinnerShown = false;
    }
  }

  //Utils for Displaying the Toaster across the Application
  showToaster(message) {
    this.toaster = this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    this.toaster.present();

  }

  //Utils for display basic alert
  showBasicAlert(title, message, callBack?) {
    let basicAlert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      enableBackdropDismiss:false,
      buttons: [{
        text: "Ok",
        cssClass: "custom_basic_alert",
        handler: () => {
          if (callBack)
            callBack();
        }
      }]
    });
    basicAlert.present();
  }

  isRealDevice() {
    if (this.platform.is('cordova')) {
      return true;
    }
    else {
      return false;
    }
  }

  processServiceTime(scheduleDetails) {
    switch (this.scheduleObj.time) {
      case 0: break;
      case 1: scheduleDetails.scheduleSlotInfo.forMorning = true;
        break;
      case 2: scheduleDetails.scheduleSlotInfo.forAfternoon = true;
        break;
      case 3: scheduleDetails.scheduleSlotInfo.forEvening = true;
        break;
      case 4: scheduleDetails.scheduleSlotInfo.forFullDay = true;
        break;
    }
  }

  //Utils for Displaying the Alert box across the Application
  showAlert(titleOfAlert, contentWithImg, message, actionButtonText, successFn, cancelFn) {
    if (contentWithImg) {
      this.alertMessage = '<div class=' + 'confirmation_message' + '>' + message.content + '</div>' + "<div class='detail_container'><div class='profile_image_container col-3'><img class='profile_image' src=" + message.imgPath + "></div>" + "<div class='detail'><div>" + message.name + "</div><div>" + message.address + "</div><div>" + message.afterAddress + "</div></div></div>";
    }
    else {
      if (message.content) {
        this.alertMessage = "<div class='confirmation_message_job_done'><span>" + message.content + "<span class='color_default'>"+" "+ message.text + "</span></span></div>";
      }
      else {
        this.alertMessage = "<div class='confirmation_message_job_done'><span>" + message + "</span> </div>";
      }
    }
    const alert = this.alertCtrl.create({
      title: titleOfAlert,
      message: this.alertMessage,
      enableBackdropDismiss:false,
      buttons: [
        {
          text: "Cancel",
          cssClass: 'alert_cancel_button',
          role: 'cancel',
          handler: () => {
            cancelFn();
          }
        },
        {
          text: actionButtonText,
          handler: () => {
            successFn();
          }
        }
      ]
    });
    alert.present();
  }



  //Utils for Displaying the Toaster across the Application
  processError(err) {
    this.hideLoading();
    var message = JSON.parse(err._body).message;
    this.toaster = this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    this.toaster.present();

  }

  resetSchedule() {
    this.scheduleObj = {
      date: new Date(),
      type: 0,
      time: 0,
      recurrence: 0,
      occurrence: 0,
      description: ''
    };
  }

  //Return the decoded JWT Token set
  decodeToken(userToken?) {
    var userToken = userToken ? userToken : this.userToken;
    return (userToken ? this.jwtHelper.decodeToken(userToken) : '');
  }

  parse_place(place, keyword) {
    // var location = [];
    var city: any;
    for (var ac = 0; ac < place.address_components.length; ac++) {
      var component = place.address_components[ac];

      switch (component.types[0]) {
        // case 'locality':
        case keyword:
          // location['city'] = component.long_name;
          city = component.long_name;
          break;
        // case 'administrative_area_level_1':
        //     location['state'] = component.long_name;
        //     break;
        // case 'country':
        //     location['country'] = component.long_name;
        //     break;
      }
    };

    return city;
  }

  processReccurence(scheduleDetails) {
    switch (this.scheduleObj.recurrence) {
      case 0: break;
      case 1:
        scheduleDetails.scheduleSlotInfo.recurrence = true;
        scheduleDetails.scheduleSlotInfo.forWeekly = true;
        break;
      case 2: scheduleDetails.scheduleSlotInfo.forMonthly = true;
        scheduleDetails.scheduleSlotInfo.recurrence = true;
        break;
    }
  }

}
